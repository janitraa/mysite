from django.shortcuts import render
from google.oauth2 import id_token
from google.auth.transport import requests
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.http import JsonResponse
from django.contrib.auth import logout as logOut

# Create your views here.
def login(request):
    if request.method == 'POST':
        try:
            
            token = request.POST['id_token']

            if 'email' in request.session:
                return JsonResponse({'status': '0', 'url': reverse('lab9:index')})
            print("dadad")
            id_info = id_token.verify_oauth2_token(token, requests.Request(), '1035338547832-lko28uh75hlfih40uoreld3o4k5o7937.apps.googleusercontent.com')
            print("cncncccc")
            if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
              
                raise ValueError('Wrong issuer.')

            print(id_info["name"])
            user_id = id_info['sub']
            name = id_info['name']
            email = id_info['email']
            request.session['user_id'] = user_id
            request.session['name'] = name
            request.session['email'] = email
            request.session['books'] = []
            name = request.user.username

            for k, v in request.session.items():
                print("{} ====== {}".format(k, v))


            return JsonResponse({'status': '0', 'url': reverse('lab9:index')})
        except ValueError as e:
            print(e)
            return JsonResponse({'status': '1'})
        except Exception as e:
            print(e)
    else:
        return render(request, 'login.html')


def logout(request):
    request.session.flush()
    print("logout")

    logOut(request)
    print("logout")
    return render(request, "login.html")

def signout(request):
    response = HttpResponseRedirect('/lab11/')
    response.delete_cookie('sessionid')
    request.session.flush()
    logOut(request)
    print("signout")
    return JsonResponse({"msg" : "logout success"})


