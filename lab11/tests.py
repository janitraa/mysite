from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import login, logout
import unittest
import time

# Create your tests here.
class Lab11_Test(TestCase):
    def test_lab_11_url_is_exist(self):
        response = Client().get('/lab11/')
        self.assertEqual(response.status_code, 200)
    
    def test_lab11_using_login_template(self):
        response = Client().get('/lab11/')
        self.assertTemplateUsed(response, 'login.html')

    def test_lab11_using_indexlab11_func(self):
        found = resolve('/lab11/')
        self.assertEqual(found.func, login)

