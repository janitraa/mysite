from django.conf.urls import url
from django.urls import path
from .views import *

#url for app
app_name='lab11'
urlpatterns = [
    url(r'^$', login, name='login'),
    path('logout/', logout, name='logout'),
    path('signout/', signout, name='signout'),

]