from django.conf.urls import url, include
from .views import index, data, get_data, add_to_favorites, remove_favorites
from django.contrib.auth import views
from django.conf import settings

#url for app
app_name = 'lab9'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^data/', data, name='data'),
    url(r'get_data/', get_data, name="get"),
    url(r'add/', add_to_favorites, name="add"),
    url(r'remove/', remove_favorites, name="remove"),
]