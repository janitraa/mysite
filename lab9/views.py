from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
import requests
import json
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt



# Create your views here.
response = {}
def index (request):
    if not request.user.is_authenticated:
        HttpResponseRedirect('/lab11/')
    if 'user_id' not in request.session.keys():
        return HttpResponseRedirect(reverse('login'))

    if 'books' in request.session.keys():
        list_book = request.session['books']
    else:
        id_info = id_token.verify_oauth2_token(token, requests.Request(), '1035338547832-lko28uh75hlfih40uoreld3o4k5o7937.apps.googleusercontent.com')
        if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            raise ValueError('Wrong issuer.')

        user_id = id_info['sub']
        # name = id_info['name']
        email = id_info['email']
        request.session['user_id'] = user_id
        # request.session['name'] = name
        request.session['email'] = email
        request.session['books'] = []        
        # list_book = []

    response['name'] = request.session['name']
    # response['count'] = len(list_book)

    return render(request, "lab9.html", response)

def data(request):
    try: 
        q = request.GET['q']
    except:
        q = 'quilting'

    getJson = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q)
    jsonParse = json.dumps(getJson.json())
    return HttpResponse(jsonParse)

def get_data(request):
    if 'user_id' not in request.session.keys():
        return HttpResponseRedirect(reverse('login'))

    if "name" in request.session:
        if(request.method == 'GET'):
            if request.session["books"] is not None:
                response["message"] = request.session["books"]
        else:
            response["message"] = "NOT ALLOWED"
    else:
        response["message"] = ""
    return JsonResponse(response)

    # book_api_url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
    # book_json = requests.get(book_api_url).json()
    # book_items = book_json['items']
    # book_list = []

    # for book in book_items:
    #     book_id = book['id']
    #     if book_id in request.session['books']:
    #         is_favorited = True
    #     else:
    #         is_favorited = False
    #     book_dict = {"url": book['volumeInfo']['imageLinks']['thumbnail'],
    #                  "title": book['volumeInfo']['title'], "authors": book['volumeInfo']['authors'][0],
    #                  "published": book['volumeInfo']['publishedDate'],
    #                  "id": book['id'], 'boolean': is_favorited}
    #     book_list.append(book_dict)
    # return JsonResponse({'data': book_list})

@csrf_exempt
def add_to_favorites(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))

    if request.method == 'POST':
        # book_id = request.POST['id']
        # if 'books' not in request.session.keys():
        #     request.session['books'] = [book_id]
        #     size = 1
        # else:
        #     books = request.session['books']
        #     books.append(book_id)
        #     request.session['books'] = books
        #     size = len(books)
        # return JsonResponse({'count': size, 'id': book_id})
        favs = request.session["books"]
        if request.POST["id"] not in favs :
            favs.append(request.POST["id"])
        request.session["books"] = favs
        response["message"] = len(favs)
        return JsonResponse(response)
    else:
        return HttpResponse("GET method is not allowed")

@csrf_exempt
def remove_favorites(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))

    if request.method == 'POST':
        # book_id = request.POST['id']
        # books = request.session['books']
        # books.remove(book_id)
        # request.session['books'] = books
        # size = len(books)
        # return JsonResponse({'count': size, 'id': book_id})
        favs = request.session["books"]
        if request.POST["id"] in favs :
            favs.remove(request.POST["id"])
        request.session["books"] = favs
        response["message"] = len(favs)
        return JsonResponse(response)
    else:
        return HttpResponse("GET method is not allowed")
        
