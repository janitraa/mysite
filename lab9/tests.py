from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
import unittest
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index, data, get_data, remove_favorites, add_to_favorites
import time

# Create your tests here.
class Lab9UnitTest(TestCase):
    def test_json_data_url_exists(self):
        response = Client().get('/lab9/data/')
        self.assertEqual(response.status_code, 200)

    def test_lab9_using_index_func(self):
        found = resolve('/lab9/')
        self.assertEqual(found.func, index)

    def test_lab9_using_data_func(self):
        found = resolve('/lab9/data/')
        self.assertEqual(found.func, data)



    


class Lab9_FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(Lab9_FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab9_FunctionalTest, self).tearDown()


