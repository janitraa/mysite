from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.db import IntegrityError
from .apps import Lab10Config
from .views import index, subscribe, validate_email
from .models import Subscribes
from .forms import SubscribeForm
import unittest

# Create your tests here.
class Lab10Test(TestCase):
    def test_lab10_url_is_exist(self):
        response = Client().get('/lab10/')
        self.assertEqual(response.status_code, 200)

    def test_lab10_using_index_func(self):
        found = resolve('/lab10/')
        self.assertEqual(found.func, index)

    def test_lab10_url_is_exist2(self):
        response = Client().get('/lab10/validate_email/')
        self.assertEqual(response.status_code,200)
        response = validate_email(HttpRequest())
        html = response.content.decode('utf8')
        self.assertIn('false', html)

    def test_post(self):
        name = 'test'
        email= 'test@testcase.com'
        password= 'test1234567'
        response_post = Client().post('/lab10/subscribe/',{'name' : name,'email':email, 'password':password})
        self.assertEqual(response_post.status_code, 200)

    def test_post2(self):
        name = 'test'
        email= ''
        password='test1234567'
        response_post = Client().get('/lab10/subscribe/',{ 'name' : name,'email':email, 'password':password})
        self.assertEqual(response_post.status_code, 200)

    def test_postf(self):
        response_post = Client().post('/lab10/subscribe/',{'name' : 'test','email':'test@gmail.com', 'password':'test1234'})
        response_post1 = Client().post('/lab10/subscribe/',{'name' : 'testjuga','email':'sama@rene.com', 'password':'test1234'})
        self.assertEqual(response_post1.status_code, 200)
