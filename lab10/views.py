from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse
from .forms import SubscribeForm
from .models import Subscribes
from django.views.decorators.csrf import csrf_exempt
from django.db import IntegrityError
import requests
import json

# Create your views here.
def index (request):
    response = {}
    form = SubscribeForm(request.POST or None)
    response['form'] = form
    return render(request, "lab10.html", response)

def subscribe(request):
    response = {}
    try:
        if (request.method == 'POST'):
            name = request.POST.get('name', None)
            email = request.POST.get('email', None)
            password = request.POST.get('password', None)
            Subscribes.objects.create(name=name, email=email, password=password)
            return HttpResponse(json.dumps({'result': "Thankyou " + name + " for subscribing!"}))
        else:
            return HttpResponse(json.dumps({'result': "fail"}))

    except IntegrityError as e:
        return HttpResponse(json.dumps({'result': 'fail'}))

@csrf_exempt
def validate_email(request):
    email = request.POST.get('email', None)
    valid = {
        'is_exist' : Subscribes.objects.filter(email=email).exists()
    }
    return JsonResponse(valid)

def list_subscriber(request):
    subscriber = Subscribes.objects.all().values()
    subs = list(subscriber)
    return JsonResponse({'subscriber' : subs})

@csrf_exempt
def unsubscribe(request):
    if (request.method == 'POST'):
        identitas = request.POST['id']
        delete = Subscribes.objects.filter(id=identitas).delete()
        return render(request, 'lab10.html')