from django.contrib import admin
from django.conf.urls import url
from .views import index, subscribe, validate_email, list_subscriber, unsubscribe

#url for app
app_name = 'lab10'
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^subscribe/', subscribe, name='subscribe'),
    url(r'^validate_email/', validate_email, name='validate_email'),
    url(r'^list_subscriber/', list_subscriber, name='list_subscriber'),
    url(r'^delete_subscriber/', unsubscribe, name='unsubscribe'),
    
]