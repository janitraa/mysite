from django import forms

class SubscribeForm(forms.Form):
    name = forms.CharField(label="Name", required=True, max_length=30, widget=forms.TextInput())
    email = forms.EmailField(label="Email", required=True, max_length=30, widget=forms.TextInput())
    password = forms.CharField(label="Password", required=True, min_length=8, max_length=12, widget=forms.PasswordInput())