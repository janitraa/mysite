from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import List_Schedule
from .forms import Schedule_Form

response = {}
# Create your views here.
def index(request):
    return render(request, 'Web.html')

def my_schedule(request):
    response['form_sched'] = List_Schedule.objects.all()
    return render(request, 'schedule.html', response)

def add_schedule(request):
    sched = Schedule_Form(request.POST)
    if request.method == "POST":
        if sched.is_valid():
            response['date'] = request.POST.get("date")
            response['time'] = request.POST.get("time")
            response['event_name'] = request.POST.get("event_name")
            response['place'] = request.POST.get("place")
            response['category'] = request.POST.get("category")
            # List_Schedule.objects.create(date=date, time=time, event_time=event_name, place=place, category=category)
            # List_Schedule.objects.save()

            form = List_Schedule(date = request.POST['date'], time = request.POST['time'], event_name = request.POST['event_name'], place = request.POST['place'], category = request.POST['category'])
            form.save()

            return render(request, 'forms.html', response)
        else:
            return render(request, 'forms.html', response)
    else:
        return render(request, 'forms.html', response)

def sched_form(request):
    response['forms'] = Schedule_Form
    return render(request, 'forms.html', response)

def delete_data(request):
    delete = List_Schedule.objects.filter().delete()
    return redirect('janitrapp:my_schedule')