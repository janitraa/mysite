from django.urls import path
from . import views

#url for app
app_name = 'janitrapp'
urlpatterns = [
    path('', views.index, name='index'),
    path('schedule/', views.my_schedule, name='my_schedule'),
    path('add-schedule/', views.add_schedule, name='add_schedule'),
    path('sched-form/', views.sched_form, name='sched_form'),
    path('delete/', views.delete_data, name='delete_data'),
] 
