from django import forms

class Schedule_Form(forms.Form):
    OPTION= (
        ('Akademis', 'Akademis'),
        ('Non-Akademis', 'Non-Akademis'),
        ('Lainnya', 'Lainnya'),
    )
    date = forms.DateField(label='Date', required=True, widget=forms.widgets.DateInput(attrs={'type' : 'date'}))
    time = forms.TimeField(label='Time', required=True, widget=forms.widgets.TimeInput(attrs={'type' : 'time'}))
    event_name = forms.CharField(label='Event Name', required=True, max_length=100)
    place = forms.CharField(label='Place', required=True, max_length=100)
    category = forms.ChoiceField(label='Category', required=True, choices=OPTION)