from django.apps import AppConfig


class JanitrappConfig(AppConfig):
    name = 'janitrapp'
