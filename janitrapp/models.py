from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class List_Schedule(models.Model):
    date = models.DateField()
    time = models.TimeField()
    event_name = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
