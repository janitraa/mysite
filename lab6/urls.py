from django.conf.urls import url
from .views import add_status, profile
#url for app
app_name = 'lab6'
urlpatterns = [
    url(r'^$', add_status, name='add_status'),
    url(r'^add/$', add_status, name='add'),
    url(r'^profile/', profile, name='profile'),
]
