from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.apps import apps
from .apps import Lab6Config
from .models import Status
from .views import add_status, profile
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time


# Create your tests here.
# bikin test buat modelsnya
# bikin test buat viewsnya

class Lab6Test(TestCase):
    def test_lab_6_url_is_exist(self):
        response = Client().get('/lab6/')
        self.assertEqual(response.status_code,200)

    def test_lab_6_using_template(self):
        response = Client().get('/lab6/')
        self.assertTemplateUsed(response, 'status.html')

    def test_lab_6_using_addstatus_func(self):
        found = resolve('/lab6/')
        self.assertEqual(found.func, add_status)

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_status = Status.objects.create(status = 'Aku baik')
    
        # Retrieving all available activity
        counting_status = Status.objects.all().count()
        self.assertEqual(counting_status, 1)

    def test_contains_hello(self):
        request = HttpRequest()
        response = add_status(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<h1>Hello, Apa kabar?</h1>', html_response)

    def test_model_can_create_new_status(self):
        example = "Hello"
        early_count = Status.objects.count()
        status_test = Status.objects.create(status=example)
        last_count = Status.objects.count()
        self.assertEqual(early_count + 1, last_count)
        self.assertEqual(status_test.status, example)

    def test_apps(self):
        self.assertEqual(Lab6Config.name, 'lab6')
        self.assertEqual(apps.get_app_config('lab6').name, 'lab6')

class ProfTest(TestCase):
    def test_profile_urls_is_exist(self):
        response = Client().get('/lab6/profile/')
        self.assertEqual(response.status_code,200)
    
    def test_profile_using_template(self):
        response = Client().get('/lab6/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_using_profile_func(self):
        found = resolve('/lab6/profile/')
        self.assertEqual(found.func, profile)

class Lab6FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(Lab6FunctionalTest, self).setUp()

    # def test_can_input_new_status(self):
    #     selenium = self.selenium
    #     selenium.get('http://janitraa.herokuapp.com/lab6/')
    #     status_box = selenium.find_element_by_id('id_status')

    #     status_box.send_keys('Coba Coba')
    #     status_box.submit()
    #     time.sleep(10)

    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    # def test_header(self):
    #     selenium = self.selenium
    #     selenium.get('http://janitraa.herokuapp.com/lab6/')
    #     header = selenium.find_element_by_tag_name('h1').text
    #     self.assertIn('Hello, Apa kabar?', header)
    #     time.sleep(2)

    def test_header_align_with_css_property(self):
        selenium = self.selenium
        selenium.get('http://janitraa.herokuapp.com/lab6/')
        content = selenium.find_element_by_tag_name('h1').value_of_css_property('text-align')
        self.assertIn('center', content)
        time.sleep(2)

    def test_header_font_with_css_property(self):
        selenium = self.selenium
        selenium.get('http://janitraa.herokuapp.com/lab6/')
        content = selenium.find_element_by_tag_name('h1').value_of_css_property('font-family')
        self.assertIn('Montserrat Alternates', content)
        time.sleep(2)

    def test_header_at_profpage(self):
        selenium = self.selenium
        selenium.get('http://janitraa.herokuapp.com/lab6/profile')
        self.assertIn('Introducing Me', selenium.page_source)

    def test_npm(self):
        selenium = self.selenium
        selenium.get('http://janitraa.herokuapp.com/lab6/profile')
        self.assertIn('NPM: 1706979316', selenium.page_source)

    def test_info(self):
        selenium = self.selenium
        selenium.get('http://janitraa.herokuapp.com/lab6/profile')
        self.assertIn('About me', selenium.page_source)
        self.assertIn('Details', selenium.page_source)

    # def test_accordion(self):
    #     selenium = self.selenium
    #     selenium.get('http://janitraa.herokuapp.com/lab6/profile')
    #     self.assertIn('Activity', selenium.page_source)
    #     self.assertIn('Organizations and Experiences', selenium.page_source)
    #     self.assertIn('Achievements', selenium.page_source)