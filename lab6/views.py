from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

# Create your views here.
# def index(request):
#     stats = Status.objects.all().values()
#     response = {}
#     response['form'] = StatusForm
#     return render(request, 'status.html', response)

def add_status(request):
    form = StatusForm(request.POST or None)
    if (request.method == 'POST'):
        if form.is_valid():
            status = request.POST.get("status")
            Status.objects.create(status=status)
            return redirect('lab6:add')
    stats = Status.objects.all().order_by("-id")
    response = {
        "stats" : stats,
        "form" : form,
    }
    return render(request, 'status.html', response)

def profile(request):
    response = {}
    return render(request, 'profile.html')