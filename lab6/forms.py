from django import forms
from .models import Status

class StatusForm(forms.Form):
    attrs = {
        'class': 'form-control',
        'placeholder': 'Apa statusmu?'
    }

    status = forms.CharField(label='Status', required=True, max_length=300, widget=forms.TextInput(attrs=attrs))