$(document).ready(function() {
	checkFavs();
	console.log("Sss");
	$("#myInput").on("keyup", function(e) {
		q = e.currentTarget.value.toLowerCase()
		console.log(q)
			$.ajax({
				url: "data/?q=" + q,
				dataType: 'json',
				success: function(data){
					$('tbody').html('')
					var stringHTML ='<tr>';
					for(i = 0; i < data.items.length; i++){
						stringHTML +=
						"<th class='align-middle' scope="+"'row'"+">" + (i+1) + "</th>" +
						"<td class='align-middle' style='text-align:center'>" + "<img id=" + data.items[i].id + " onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td>" +
						"<td class='align-middle'><img src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
						"<td class='align-middle'>" + data.items[i].volumeInfo.categories +"</td>" + 
						"<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
						"<td class='align-middle'>" + data.items[i].volumeInfo.authors +"</td>" + 
						"<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td></tr>";
					} 
					$('tbody').append(stringHTML);
					checkFavs();
				},
				error: function(error){
					alert("Books not found");
				}
			})
	});
});

var counter = 0;
function favorite(clicked_id){
	var btn = document.getElementById(clicked_id);
	if (btn.classList.contains("checked")){
		btn.classList.remove("checked");
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
		counter--;
		var count = document.getElementById("counter").innerHTML = counter;
	}
	else{
		btn.classList.add('checked');
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
		counter++;
		var count = document.getElementById("counter").innerHTML = counter;
	}
}

function checkFavs(){
	$.ajax({
		type: 'GET',
		url: '/lab9/get_data/',
		dataType: 'json',
		success: function(data) {
			console.log(data);
			for (var i=1; i <= data.message.length; i++) {
				console.log(data.message[i-1]);
				var id = data.message[i-1];
				var td = document.getElementById(id);
				if (td!=null) {
					td.className = 'clicked';
					td.src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
				}  
				$('#counter').html(data.message.length);	  
			}
		}
	});  
};
  
function favorite(id) {
	var csrftoken = $("[name=csrfmiddlewaretoken]").val();
	var ini = document.getElementById(id);
	var yellowstar = 'https://image.flaticon.com/icons/svg/291/291205.svg';
	var blankstar = 'https://image.flaticon.com/icons/svg/149/149222.svg';
	if (ini.className=='checked') {
		$.ajax({
			url: "/lab9/remove/",
			type: "POST",
			headers: {
				"X-CSRFToken": csrftoken,
			},	
			data: {
			id: id,
			},
			success: function(result) {
				counter=result.message;
				ini.className='';
				ini.src=blankstar;
				$('#counter').html(counter);
			},
			error : function (errmsg){
				alert("Something is wrong");
			}
		});
	} else {
		$.ajax({
			url: "/lab9/add/",
			type: "POST",
			headers: {
				"X-CSRFToken": csrftoken,
			},	
			data: {
				id: id,
			},
			success: function(result) {
				console.log(ini);
				counter=result.message;
				ini.className='checked';
				ini.src=yellowstar;
				$('#counter').html(counter);
			   
		   },
		   error : function (errmsg){
			   alert("Something is wrong");
		   }
		});
	}
}


// function getData() {
//     var csrftoken = $("[name=csrfmiddlewaretoken]").val();
//     var html;

//     $.ajax({
//         method: 'GET',
//         url: '/get_data/',
//         headers: {
//             "X-CSRFToken": csrftoken,
//         },
//         success: function (result) {
//             var data = result.data;
//             var icon;
//             for (var i = 0; i < data.length; i++) {
//                 var book_info = data[i];
//                 if (book_info.boolean) {
//                     icon = `<button id="${book_info['id']}" onclick='removeFavorites("${book_info['id']}")'><i class="yellow"></i></button>`;
//                 } else {
//                     icon = `<button id="${book_info['id']}" onclick='addFavorites("${book_info['id']}")'><i class="def"></i></button>`;
//                 }
//                 html =
//                     `<tr><td align='center'><img width='80' height='100' src='${book_info['url']}'></td><td>${book_info['title']}</td><td>${book_info['authors']}</td><td>${book_info['published']}</td><td align="center">${icon}</td></tr>`;
//                 $('#tbody').append(html);
//             }
//         }
//     })
// }

// function addFavorites(id) {
//     var csrftoken = $("[name=csrfmiddlewaretoken]").val();
//     $.ajax({
//         method: "POST",
//         url: "/add/",
//         headers: {
//             "X-CSRFToken": csrftoken,
//         },
//         data: {
//             id: id,
//         },
//         success: function (res) {
//             var icon = `<button id="${res.id}" onclick='removeFavorites("${res.id}")'><i class="yellow"></i></button>`;
//             $("#" + res.id).replaceWith(icon);
//             $("#counter").replaceWith(`<span id='counter'>${res.count}</span>`);
//         },
//         error: function () {
//             alert("Error, cannot get data from server")
//         }
//     });
// }

// function removeFavorites(id) {
//     var csrftoken = $("[name=csrfmiddlewaretoken]").val();
//     $.ajax({
//         method: "POST",
//         url: "/remove/",
//         headers: {
//             "X-CSRFToken": csrftoken,
//         },
//         data: {
//             id: id,
//         },
//         success: function (res) {
//             var icon = `<button id="${res.id}" onclick='addFavorites("${res.id}")'><i class="def"></i></button>`;
//             $('#' + res.id).replaceWith(icon);
//             $("#counter").replaceWith(`<span id='counter'>${res.count}</span>`);
//         },
//         error: function () {
//             alert("Error, cannot get data from server")
//         }
//     });
// }


