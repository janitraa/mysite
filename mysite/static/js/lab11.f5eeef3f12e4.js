// var GoogleAuth; // Google Auth object.
// function initClient() {
//     gapi.client.init({
//         'apiKey': 'VZc6H-4ZMZ6T_VwqEqu7pkA_',
//         'clientId': '1035338547832-lko28uh75hlfih40uoreld3o4k5o7937.apps.googleusercontent.com',
//         'scope': 'https://www.googleapis.com/auth/drive.metadata.readonly',
//         'discoveryDocs': ['https://www.googleapis.com/discovery/v1/apis/drive/v3/rest']
//     }).then(function () {
//         GoogleAuth = gapi.auth2.getAuthInstance();
  
//         // Listen for sign-in state changes.
//         GoogleAuth.isSignedIn.listen(updateSigninStatus);
//     });
//   }

//   function updateSigninStatus(isSignedIn) {
//     setSigninStatus();
//   }
//   function setSigninStatus(isSignedIn) {
//     var user = GoogleAuth.currentUser.get();
//     var isAuthorized = user.hasGrantedScopes(SCOPE);
//     if (isAuthorized) {
//       $('#sign-in-or-out-button').html('Sign out');
//       $('#revoke-access-button').css('display', 'inline-block');
//       $('#auth-status').html('You are currently signed in and have granted ' +
//           'access to this app.');
//     } else {
//       $('#sign-in-or-out-button').html('Sign In/Authorize');
//       $('#revoke-access-button').css('display', 'none');
//       $('#auth-status').html('You have not authorized this app or you are ' +
//           'signed out.');
//     }
//   }

$(document).ready(function () {
    gapi.load('auth2', function () {
        gapi.auth2.init({
            client_id: 1035338547832-lko28uh75hlfih40uoreld3o4k5o7937.apps.googleusercontent.com,
        });
    });
  });

function onSignIn(googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    var profile = googleUser.getBasicProfile();
    console.log(profile.getName());
    sessionStorage.setItem('user', profile.getName());
    sendToken(id_token);
    console.log(id_token);
    console.log("Logged In");
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.disconnect();
    window.location.href = "/lab11/";
    console.log('User signed out.');
    // auth2.signOut().then(function () {
    //     console.log('User signed out.');
    // });
}

function sendToken(token) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var html;

    $.ajax({
        method: "POST",
        url: "/lab11/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {id_token: token},
        success: function (result) {
            console.log("Successfully send token");
            console.log(result);
            if (result.status === "0") {
                html = "<h4 class='success'>Logged In</h4>";
                window.location.replace(result.url);
            } else {
                html = "<h4 class='fail'>Something fail, please report</h4>"
            }
            $("h4").replaceWith(html)
        },
        error: function () {
            alert("Something error, please report")
        }
    })
}
