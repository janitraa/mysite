$(document).ready(function() {
	$("#myInput").on("keyup", function(e) {
		q = e.currentTarget.value.toLowerCase()
		console.log(q)
			$.ajax({
				url: "data/?q=" + q,
				dataType: 'json',
				success: function(data){
					$('tbody').html('')
					var stringHTML ='<tr>';
					for(i = 0; i < data.items.length; i++){
						stringHTML +=
						"<th class='align-middle' scope="+"'row'"+">" + (i+1) + "</th>" +
						"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td>" +
						"<td class='align-middle'><img src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
						"<td class='align-middle'>" + data.items[i].volumeInfo.categories +"</td>" + 
						"<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
						"<td class='align-middle'>" + data.items[i].volumeInfo.authors +"</td>" + 
						"<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td></tr>";
					} 
					$('tbody').append(stringHTML);
				},
				error: function(error){
					alert("Books not found");
				}
			})
	});
});

var counter = 0;
function favorite(clicked_id){
	var btn = document.getElementById(clicked_id);
	if (btn.classList.contains("checked")){
		btn.classList.remove("checked");
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
		counter--;
		var count = document.getElementById("counter").innerHTML = counter;
	}
	else{
		btn.classList.add('checked');
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
		counter++;
		var count = document.getElementById("counter").innerHTML = counter;
	}
}

