var csrftoken = getCookie('csrftoken');
function getCookie(c_name){
    if(document.cookie.length>0){
        c_start = document.cookie.indexOf(c_name + "=");
        if(c_start != -1){
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}

function safeMethod(method){
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function validationEmail(email){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

$(document).ready(function(){
    $.ajaxSetup({
        beforeSend: function(xhr, settings){
            if(!safeMethod(settings.type) && !this.crossDomain){
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

$("form").submit(function(event){
    $.ajax({
        method: "POST",
        url: 'subscribe/',
        dataType: 'json',
        data: {
            'name' : $("#id_name").val(),
            'email' : $("#id_email").val(),
            'password' : $("#id_password").val(),
        },
        success: function(message){
            $("#dialog").dialog();
                $("#dialog").html("<p>" + message['result'] + "</p>");
                $("#id_name").val('');
                $("#id_email").val('');
                $("#id_password").val('');
        },
        error: function(message){
            alert('Email not sent');
        },
    }),
    event.preventDefault();
});

$("#id_email").change(function(){
    email = $(this).val();
    $.ajax({
        method: "POST",
        url: 'validate_email/',
        dataType: 'json',
        data: {
            'email': email
        },
        success: function(data){
            if(validationEmail(email) == false){
                $("#id_email").css("background-color", "red");
                $("#submit")[0].disabled = true;
                $("#wait").html("Enter a valid email address");                
            }
            else if(data.is_exist){
                alert("Email is already exist");
                $("#id_email").css("background-color", "red");
                $("#submit")[0].disabled = true;
                $("#wait").html("Use another email address");  
            }
            else{
                $("#id_email").css("background-color", "white");
                $("#submit")[0].disabled = false;
                $("#wait").html("");  
            }
        }
    });
})
})