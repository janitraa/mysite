var csrftoken = getCookie('csrftoken');
function getCookie(c_name){
    if(document.cookie.length > 0){
        c_start = document.cookie.indexOf(c_name + "=");
        if(c_start != -1){
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

function csrftokenSafeMethod(method){
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function validEmail(email){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function isValid(){
    if(($("#id_email").val()==null || $("#id_email").val()=="") || ($("#id_password").val()==null || $("#id_password").val()=="") || 
    ($("#id_name").val()==null || $("#id_name").val()=="")){
        return false;
    }
    return true;
}

$(document).ready(function(){
    $.ajaxSetup({
        beforeSend: function(xhr, settings){
            if(!csrftokenSafeMethod(settings.type) && !this.crossDomain){
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

$("form").submit(function(event){
    $.ajax({
        method: "POST",
        url: 'subscribe/',
        data: {
            'name' : $("#id_name").val(),
            'email' : $("#id_email").val(),
            'password' : $("#id_password").val(),
        },
        dataType: 'json',
        success: function(msg){
            $( "#dialog" ).dialog();
                $( "#dialog" ).html("<p>"+msg['result']+"</p>");
                $("#id_name").val('');
                $("#id_email").val('');
                $("#id_password").val('');
        },
            error: function(msg){
                alert('Email not sent');
        },

    }),
    event.preventDefault();
});

$("#id_name").change(function(){
    if(isValid() && valid){
        $("#submit")[0].disabled=false;
    }
    else{
        $("#submit")[0].disabled=true;
    }
});

$("#id_password").change(function(){
    if(isValid() && valid){
        $("#submit")[0].disabled=false;
    }
    else{
        $("#submit")[0].disabled=true;
    }
});

$("#id_email").change(function(){
    email = $(this).val();
    $.ajax({
        method: "POST",
        url: 'validate_email/',
        data: {
            'email': email
        },
        dataType: 'json',
        success: function(data){
            if(validEmail(email)==false){
                $('#id_email').css("border-color", "red");
                $("#submit")[0].disabled=true;
                $('#wait').html('Enter a valid email address');
            }
            else if (data.is_exist) {
                alert("Email is already exist");
                $('#id_email').css("border-color", "red");
                $("#submit")[0].disabled=true;
                $('#wait').html('Use another email address');
            }
            else{
                $('#id_email').css("border-color", "white");
                $("#submit")[0].disabled=false;
                $('#wait').html('');
            }
        }
    });
})
})
